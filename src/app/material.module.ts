import { NgModule } from '@angular/core';

import {MatToolbarModule} from '@angular/material/toolbar';
import {MatPaginatorModule, MatPaginatorIntl} from '@angular/material/paginator';
import {MatButtonModule} from '@angular/material/button';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatCardModule} from '@angular/material/card';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatIconModule} from '@angular/material/icon';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatSnackBarModule} from '@angular/material/snack-bar';

// import {CdkTableModule} from '@angular/cdk/table';

// ----------------- traduction de la pagination ---------------------
// Text nombre d'items
const textRangeLabel = (page: number, pageSize: number, length: number) => {
  if (length == 0 || pageSize == 0) return `0 de ${length}`;

  length = Math.max(length, 0);

  const startIndex = page * pageSize;

  // Si l'index de début dépasse la longueur de la liste, ne pas corriger l'index de fin à la fin.
  const endIndex = startIndex < length ? Math.min(startIndex + pageSize, length) : startIndex + pageSize;

  return `${startIndex + 1} - ${endIndex} de ${length}`;
}

// Tous les text
export function getTradPagination() {
  const paginatorIntl = new MatPaginatorIntl();

  paginatorIntl.itemsPerPageLabel = 'Items par page:';
  paginatorIntl.firstPageLabel = 'Première page';
  paginatorIntl.nextPageLabel = 'Page suivante';
  paginatorIntl.previousPageLabel = 'Page précédente';
  paginatorIntl.lastPageLabel = 'Dernière page';
  paginatorIntl.getRangeLabel = textRangeLabel;

  return paginatorIntl;
}

@NgModule({
  exports: [
    MatToolbarModule,
    MatPaginatorModule,
    MatButtonModule,
    MatGridListModule,
    MatCardModule,
    FlexLayoutModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatSlideToggleModule,
    MatSnackBarModule
    // CdkTableModule,
  ],
  providers: [
    { provide: MatPaginatorIntl, useValue: getTradPagination() }
  ]
})

export class MaterialModule { }
