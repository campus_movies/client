import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Movie } from '../models/movie';

@Injectable({
  providedIn: 'root'
})
export class MovieService {

  constructor(private httpClient: HttpClient) {}

  saveMultipleMovies(movies: Movie[]): Observable<void> {
    return this.httpClient.post<void>(environment.apiUrl + '/movie/add', movies);
  }

  getAllMovies(): Observable<Movie[]> {
    return this.httpClient.get<Movie[]>(environment.apiUrl + '/movie/all');
  }
}
