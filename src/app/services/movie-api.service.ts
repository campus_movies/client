import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { MovieResult } from '../models/movie-result';

@Injectable({
  providedIn: 'root'
})
export class MovieApiService {

  constructor(private httpClient: HttpClient) { }

  getPopularMovies(page: number): Observable<MovieResult> {
    const params = new HttpParams({
      fromObject: {
        api_key: environment.apiTmdbKey,
        page,
        language: 'fr-FR'
      }
    });

    return this.httpClient.get<MovieResult>(environment.apiTmdbUrl + '/movie/popular', {params});
  }

  getSearchMovies(search: string, page: number) {
    const params = new HttpParams({
      fromObject: {
        api_key: environment.apiTmdbKey,
        query: search,
        page,
        language: 'fr-FR'
      }
    });

    return this.httpClient.get<MovieResult>(environment.apiTmdbUrl + '/search/movie', {params});
  }

}
