import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListCardMoviesComponent } from './list-card-movies.component';

describe('ListCardMoviesComponent', () => {
  let component: ListCardMoviesComponent;
  let fixture: ComponentFixture<ListCardMoviesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListCardMoviesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListCardMoviesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
