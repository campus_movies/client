import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Movie } from 'src/app/models/movie';

@Component({
  selector: 'app-list-card-movies',
  templateUrl: './list-card-movies.component.html',
  styleUrls: ['./list-card-movies.component.scss']
})
export class ListCardMoviesComponent implements OnInit {

  apiTmdbImageUrl = environment.apiTmdbImagesUrl

  private _moviesSelected: Movie[] = [];
  private _movies: Movie[] = [];

  @Input() set moviesSelected(listMoviesSelect: Movie[]) {
    this.movies.forEach(m => {
      m.selected = listMoviesSelect.some(ms => ms.id === m.id) ? true : false;
    });

    this._moviesSelected = listMoviesSelect;
  }

  get moviesSelected(): Movie[] {
    return this._moviesSelected;
  }

  @Output() moviesSelectedChange = new EventEmitter<Movie[]>();

  @Input() set movies(listMovies: Movie[]) {
    listMovies.forEach(m => {
      m.selected = this.moviesSelected.some(ms => m.id === ms.id);
    });

    this._movies = listMovies;
  }

  get movies(): Movie[] {
    return this._movies;
  }

  @Output() moviesChange = new EventEmitter<Movie[]>();

  @Input() isSelectedMode = false;

  constructor() { }

  ngOnInit(): void { }

  // =================================================================================
  //                                Sélection
  // =================================================================================
  onSelectCard(movie: Movie) {
    movie.selected = !movie.selected;

    movie.selected ? this.addSelectedMovie(movie) : this.removeSelectedMovie(movie);

    this.moviesSelectedChange.emit(this.moviesSelected);
  }

  addSelectedMovie(movie: Movie) {
    this.moviesSelected.push(movie);
  }

  removeSelectedMovie(movie: Movie) {
    this.moviesSelected = this.moviesSelected.filter(m => m.id !== movie.id);
  }

}
