import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HeaderComponent } from './containers/header/header.component';
import { MaterialModule } from './material.module';
import { MaBibliotequeComponent } from './containers/ma-biblioteque/ma-biblioteque.component';
import { TmdbBibliotequeComponent } from './containers/tmdb-biblioteque/tmdb-biblioteque.component';
import { ListCardMoviesComponent } from './components/list-card-movies/list-card-movies.component';
import { NoMoviesComponent } from './components/no-movies/no-movies.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MaBibliotequeComponent,
    TmdbBibliotequeComponent,
    ListCardMoviesComponent,
    NoMoviesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
