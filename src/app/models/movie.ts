export class Movie {
  id: number;
  poster_path: string;
  title: string;
  overview: string;
  selected: boolean;

  constructor(id: number, poster_path: string, title: string, overview: string, selected: boolean = false) {
    this.id = id;
    this.poster_path = poster_path;
    this.title = title;
    this.overview = overview;
    this.selected = selected;
  }
}
