import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MaBibliotequeComponent } from './containers/ma-biblioteque/ma-biblioteque.component';
import { TmdbBibliotequeComponent } from './containers/tmdb-biblioteque/tmdb-biblioteque.component';

const routes: Routes = [
  {
    path: 'ma-bibliotheque',
    component: MaBibliotequeComponent,
  },
  {
    path: 'tmdb-bibliotheque',
    component: TmdbBibliotequeComponent
  },
  {
    path: '**',
    redirectTo: 'ma-bibliotheque'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
