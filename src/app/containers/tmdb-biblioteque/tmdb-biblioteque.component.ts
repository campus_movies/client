import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { Movie } from 'src/app/models/movie';
import { MovieResult } from 'src/app/models/movie-result';
import { MovieApiService } from 'src/app/services/movie-api.service';
import { MovieService } from 'src/app/services/movie.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-tmdb-biblioteque',
  templateUrl: './tmdb-biblioteque.component.html',
  styleUrls: ['./tmdb-biblioteque.component.scss']
})
export class TmdbBibliotequeComponent implements OnInit, OnDestroy {

  search: string = '';
  movies: Movie[] = [];

  pageSize = 20;
  pageLength = 0;
  pageIndex = 0;

  isSelectedMode = false;
  moviesSelected: Movie[] = [];

  subscriptions: Subscription[] = [];

  @ViewChild('paginator') paginator!: MatPaginator;

  constructor(private movieApiService: MovieApiService, private movieService: MovieService, private snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.getPopularMovies(1);
  }

  // =================================================================================
  //                            Récupération films
  // =================================================================================
  getPopularMovies(page: number) {
    this.subscriptions.push(
      this.movieApiService.getPopularMovies(page).subscribe((data: MovieResult) => {
        this.movies = data.results;
        this.pageLength = data.total_results;
      })
    );
  }

  getSearchMovies(search: string, page: number) {
    this.subscriptions.push(
      this.movieApiService.getSearchMovies(search, page).subscribe((data: MovieResult) => {
        this.movies = data.results;
        this.pageLength = data.total_results;
      })
    )
  }

  getMovies(page: number) {
    if (this.search === '') {
      this.getPopularMovies(page);
    } else {
      this.getSearchMovies(this.search, page);
    }
  }

  // =================================================================================
  //                                Pagination
  // =================================================================================
  changePage(page: PageEvent) {
    this.pageSize = page.pageSize;
    this.pageIndex = page.pageIndex;
    this.pageLength = page.length;

    this.getMovies(this.pageIndex + 1);
  }

  // =================================================================================
  //                                Sélection
  // =================================================================================
  switchMode() {
    this.isSelectedMode = !this.isSelectedMode;
  }

  saveSelection() {
    this.subscriptions.push(
      this.movieService.saveMultipleMovies(this.moviesSelected).subscribe(data => {
        this.snackBar.open('Ajout effectué', 'x', {
          horizontalPosition: 'center',
          verticalPosition: 'top',
          duration: 2500
        });
        this.resetSelectMovies();
      })
    );

  }

  resetSelectMovies() {
    this.isSelectedMode = false;
    this.moviesSelected = [];
  }

  // =================================================================================
  //                                Recherche
  // =================================================================================
  searchMovies() {
    this.paginator.firstPage();
    this.getMovies(this.pageIndex + 1);
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(subscription => {
      subscription.unsubscribe();
    });
  }

}
