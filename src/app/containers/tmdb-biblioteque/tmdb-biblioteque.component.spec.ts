import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatSnackBar, MatSnackBarModule } from '@angular/material/snack-bar';

import { TmdbBibliotequeComponent } from './tmdb-biblioteque.component';

describe('TmdbBibliotequeComponent', () => {
  let component: TmdbBibliotequeComponent;
  let fixture: ComponentFixture<TmdbBibliotequeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TmdbBibliotequeComponent ],
      imports: [ HttpClientModule, MatSnackBarModule ],
      providers: [
        // {provide: MatSnackBar, useValue: 'matSnackBar'}
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TmdbBibliotequeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
