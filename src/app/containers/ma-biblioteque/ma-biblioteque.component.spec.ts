import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MaBibliotequeComponent } from './ma-biblioteque.component';

describe('MaBibliotequeComponent', () => {
  let component: MaBibliotequeComponent;
  let fixture: ComponentFixture<MaBibliotequeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MaBibliotequeComponent ],
      imports: [HttpClientModule]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MaBibliotequeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
