import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { Movie } from 'src/app/models/movie';
import { MovieService } from 'src/app/services/movie.service';

@Component({
  selector: 'app-ma-biblioteque',
  templateUrl: './ma-biblioteque.component.html',
  styleUrls: ['./ma-biblioteque.component.scss']
})
export class MaBibliotequeComponent implements OnInit, OnDestroy {

  constructor(private movieService: MovieService) { }

  movies: Movie[] = [];
  subscriptions: Subscription[] = [];

  ngOnInit(): void {
    this.subscriptions.push(
      this.movieService.getAllMovies().subscribe((data: Movie[]) => {
        this.movies = data;
      })
    );
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(subscription => {
      subscription.unsubscribe();
    });
  }

}
