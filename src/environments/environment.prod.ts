export const environment = {
  production: true,
  apiUrl: 'http://localhost:3001',
  apiTmdbUrl: 'https://api.themoviedb.org/3',
  apiTmdbKey: '827a095180e99458b40a0b50a91b837c',
  apiTmdbImagesUrl: 'https://image.tmdb.org/t/p/w500'
};
